import json
import pandas as pd
import re
from datetime import datetime
import pytz
import sys

def date_string_to_timestamp(date_string):
    # Define the date format
    date_format = "%b %d, %Y %I:%M:%S %p"

    # Parse the date string
    dt_object = datetime.strptime(date_string, date_format)

    # Convert to timestamp
    timestamp = dt_object.timestamp()

    return timestamp

def filter_string_to_timestamp(date_string):
    # Define the date format
    date_format = "%m/%d/%Y"

    # Parse the date string
    dt_object = datetime.strptime(date_string, date_format)

    # Convert to timestamp
    timestamp = dt_object.timestamp()

    return timestamp

def remove_html_tags(text):  
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)

if len(sys.argv) > 3:
    print("Incorrect number of arguements, must have two dates in mm/dd/YYYY format")
    quit()

opps = []
count = 0

with open("opps_export.txt") as input:
    for jsonObj in input:
        opps.append(pd.json_normalize(json.loads(jsonObj)))
        count += 1

if len(sys.argv) > 1:
    if len(sys.argv) > 2:
        low = filter_string_to_timestamp(str(sys.argv[1]))
    else:
        low = 0
    high = filter_string_to_timestamp(str(sys.argv[2]))

    #print(opps)

    for x in range(len(opps)):
        #print(count - x - 1)
        data = opps[count - x - 1]
        #print(data.loc[0].at['synopsis.responseDate'][:-3])
        #print(data.loc[0].at['synopsis.postingDate'][:-4])
        time = date_string_to_timestamp(data.loc[0].at['synopsis.postingDate'][:-4])
        if time < low or time > high or data.loc[0].at['synopsis.agencyDetails.topAgencyCode'] == 'HHS':
            #print(opps[i].loc[0].at['synopsis.responseDate'][:-4])
            opps.pop(count - x - 1)
            #count -= 1

    count = len(opps)


#frame = [[count] * 15] * count
frame = [[]] * count

thing = 0
for i in frame:
    frame[thing] = [thing] * 15
    thing += 1
#print(frame)
num = 0

#opps = opps.loc[opps['']]

for data in opps:
    #print(frame[0])
    for index, row in data.iterrows():
        row['assistURL'] = "=HYPERLINK(\"https://grants.gov/search-results-detail/" + str(row['id']) + "\",\"" + str(row['opportunityNumber']) + "\")"
    frame[num][0] = "=HYPERLINK(\"https://grants.gov/search-results-detail/" + str(data.loc[0].at['id']) + "\",\"" + str(data.loc[0].at['opportunityNumber']) + "\")"
    #print(frame[0])
    frame[num][1] = data.loc[0].at['opportunityTitle']
    #print(data.loc[0].at['opportunityTitle'])
    frame[num][2] = data.loc[0].at['synopsis.agencyCode']
    frame[num][3] = data.loc[0].at['synopsis.agencyDetails.agencyName']
    if 'synopsis.responseDate' in data:
        frame[num][4] = data.loc[0].at['synopsis.responseDate']
    else:
        frame[num][4] = ""
    if 'synopsis.awardCeiling' in data:
        frame[num][5] = data.loc[0].at['synopsis.awardCeiling']
    else:
        frame[num][5] = ""
    if 'synopsis.awardFloor' in data:
        frame[num][6] = data.loc[0].at['synopsis.awardFloor']
    if 'synopsis.numberOfAwards' in data:
        frame[num][7] = data.loc[0].at['synopsis.numberOfAwards']
    else:
        frame[num][7] = ""
    frame[num][8] = remove_html_tags(data.loc[0].at['synopsis.synopsisDesc'])
    if 'synopsis.applicantTypes' in data: 
        frame[num][9] = data.loc[0].at['synopsis.applicantTypes'][0]['description']
        #print(type(frame[num][9]))
    else:
        print("error")
    if 'synopsis.applicantEligibilityDesc' in data:
        frame[num][10] = data.loc[0].at['synopsis.applicantEligibilityDesc']
    else:
        frame[num][10] = ""
    #print(frame[count][1])
    num += 1
#print(frame)
out = pd.DataFrame(frame)
out.to_csv("opps_export.csv", header=False, index=False)