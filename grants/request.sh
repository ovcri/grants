#!/bin/bash

filename=opps_export.txt
idfile=ids.txt
if test -f $filename; then
    rm $filename
fi 

curl --silent -X POST "https://apply07.grants.gov/grantsws/rest/opportunities/search" -H "Host: apply07.grants.gov" -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0" -H "Accept: application/json, text/plain, */*" -H "Accept-Language: en-US,en;q=0.5" -H "Accept-Encoding: gzip, deflate, br" -H "Content-Type: application/json" -H "Origin: https://www.grants.gov" -H "Connection: keep-alive" -H "Referer: https://www.grants.gov/" -H "Sec-Fetch-Dest: empty" -H "Sec-Fetch-Mode: cors" -H "Sec-Fetch-Site: same-site" --data '{"keyword":null,"oppNum":null,"cfda":null,"agencies":null,"sortBy":"openDate|desc","rows":5000,"eligibilities":"06|99|25","fundingCategories":null,"fundingInstruments":null,"dateRange":"","oppStatuses":"posted"}' 2>/dev/null | jq '.oppHits[].id' | sed 's/"//g' > $idfile
echo "Opportunity Ids downloaded"

while read id; do
    echo "Getting Opportunity $id"
    bash request_id.sh $id $filename
done < $idfile
rm $idfile