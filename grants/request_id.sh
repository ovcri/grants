#!/bin/bash

id=$1
curl --silent -X POST "https://apply07.grants.gov/grantsws/rest/opportunity/details" -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0" -H "Accept: */*" -H "Accept-Language: en-US,en;q=0.5" -H "Accept-Encoding: gzip, deflate, br" -H "Content-Type: application/x-www-form-urlencoded;charset=UTF-8" -H "Origin: https://grants.gov" -H "Connection: keep-alive" -H "Referer: https://grants.gov/" -H "Sec-Fetch-Dest: empty" -H "Sec-Fetch-Mode: cors" -H "Sec-Fetch-Site: same-site" --data "oppId=$id" >> $2
echo "" >> $2
echo "Opportunity $id downloaded"
