# Funding scripts

This repo contains several scripts to help with automating the process of
downloading and formatting grants.gov data and PIVOT-RP data.

## <a href="./download.py">download.py</a>

Automatically download from grants.gov. It only retrieves posted opportunities,
and opportunities for "Public and state controlled institutions" and "Others".


```bash
python3 download.py
```

## <a href="./download_opportunity.py">download_opportunity.py</a>
Automatically retrieve the HTML document linked to by the hyperlink.

```bash
python3 download.py | python3 download_opportunity.py
```

The output is printed out to stdout in the same format as received, populating
the HTML fields. Each row from the input file takes about 3 seconds to process.

## <a href="./extract_attributes.py">extract_attributes.py</a>

1. Award Floor
2. Award Ceiling

The output is printed to stdout in csv format the award ceiling and award floor.

```bash
python3 download.py | python3 download_opportunity.py | python3 extract_attributes.py > all_federal_opportunities.csv
```

## <a href="./fed_select_grants.csv">fed_select_grants.csv</a>

Given a file as a command line argument (output from extract_oppornity.py), iterate
through each entry, and allow user to either Add the opp to selected opps,
Remove the opps from selected opps, or Review the opp for further review. Upon
exiting, the file is updated

```bash
python3 fed_select_grants.csv all_federal_opportunities.csv
```

The output is in csv format printed to stdout.

## <a href="./process_all_federal_grants.py">./process_all_federal_grants.py</a>
This script accepts the a downloaded csv from from grants.gov and prepares the
file to be exported to excel.

It implements part of the procedure as outlined in

*Monthly Procedure for PIVOT Fund Opportunities*

Specifically, the script selects all opportunities with closing dates between
a start date provided as a command line argument, and 90 days after the start date

```
cat all_grants.csv | python3 process_all_federal_grants.py 01/01/2022 --header > all_federal_grants.csv
```

The generated csv file needs to be imported to excel for further formatting.


## <a href="./process_pivot.py">process_pivot.py</a>

