Selected Federal Fund Opportunities Excluding HHS
=================================================

1. Create a folder in SDRIVE and place last month's file into the folder

2. Convert last month's excel file to csv

        .. code-block:: bash
                
                excel_to_csv $EXCEL_FILE $LAST_MONTH_CSV


3. Download csv file from grants.gov, filter out opps, and add additional attribute data

        .. code-block:: bash
                
                fed_download \
                | fed_filter posted-date --start=$POSTED_DATE \
                | fed_filter close-date --start=$CLOSE_DATE \
                | fed_filter agency-code 'HHS' --exclude \
                | fed_filter agency 'U\.S\. Mission' --exclude \
                | fed_filter title 'U\.S\. Mission' --exclude \
                | fed_download_opps \
                | fed_extract_attr \
                > tmp1.txt


4. Remove any opportunities previously listed

        .. code-block:: bash
                
                fed_set difference $LAST_MONTH_CSV < tmp1.txt > tmp2.csv

5. Tag opportunities to be added or removed or reviewed

        .. code-block:: bash
                
                fed_select tmp2.csv


6. Filter out any opp not tagged with Add or Review

        .. code-block:: bash
                
                fed_filter select 'Add|Review' < tmp2.csv > tmp3.csv

7. Delete unwanted columns

        .. code-block:: bash
                
                fed_to_excel --header < tmp3.csv > selected.csv

8. Convert to Excel Workbook
        
        .. code-block:: bash
                
                 csv_to_excel selected.csv "Selected Federal Opportunities Excluding HHS (as of $DATE).xlsx" --header

9. Open the Excel file and highlight all rows tagged with "Review" and delete the column
10. Do additional formatting to make it look like last months file

