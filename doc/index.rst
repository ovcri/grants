.. Grant Scripts documentation master file, created by
   sphinx-quickstart on Fri Dec 31 14:45:04 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Grant Scripts's documentation!
=========================================

This package provides several scripts to automate or greatly expedite the process
of downloading and selecting grants.



.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   ssh.rst
   script.rst
   selected_federal_grants.rst
   selected_non_federal_opps.rst   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
