
Selected Non-Federal Fund Opportunities
=======================================


1. Create a folder in SDRIVE

2. Download Opps from PIVOT-RP
3. Join the files

        .. code-block:: bash
                
                # Strip the header of file1 and create opps.csv
                'tail -n +2' < "file1" > opps.csv

                # Strip the header of file2 and  append to opps.csv  
                tail -n +2 < "file2" >> opps.csv
4. Convert ``opps.csv`` CSV files into Federal grants format and filter opps

        .. code-block:: bash
                
                # Convert to fed csv
                piv_to_fed < 'opps.csv' \
                | piv_extract_attr \
                | fed_filter close-date --start=$CLOSE_DATE \
                | fed_filter amount --low=500000 \
                > tmp1.csv
            
5. Select Grants
        
        .. code-block:: bash
                
                fed_select tmp1.csv

6. Strip out columns and add column header
        
        .. code-block:: bash
                
                piv_to_excel --header < tmp1.csv > tmp2.csv

7. Convert CSV to Excel Workbook

        .. code-block:: bash
            
                csv_to_excel tmp2.csv "Selected Non-Federal Fund Opportunities Available in 3 Months (as of $DATE).xlsx" --header 

8. Open the workbook file and format for appearance.

 



