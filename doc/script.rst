Scripts
=======

************
fed_download
************
Download posted grants for "Public Institutions..." and "Other" as CSV file from grants.gov

.. code-block:: bash
        
        $ fed_download

To save to file:

.. code-block:: bash
        
        $ fed_download > grants.csv

**********
fed_filter
**********

Filter out grants based on fields

.. code-block:: bash

        # Pipe to 'grants.csv' fed_filter and print out only the grants posted after 11/01/2021
        $ cat grants.csv | fed_filter posted-date --start=11/01/2021
        
        #  Pipe 'grants.csv' to fed_filter and print out grants with close date after 01/01/2022
        #  and before 02/01/2022
        $ cat grants.csv | fed_filter close_date --start=01/01/2022 --end=02/01/2022

        # Exclude grants with 'HHS' in agency code
        $ cat grants.csv | fed_filter agency-code 'HHS' --exclude

        # Select grants with 'NFS' in agency code
        $ cat grants.csv | fed_filter agency-code 'NSF'


********
fed_sort
********

Sort based on column and print

.. code-block:: bash

        # Sort based on earliest close date
        $ cat grants.csv | fed_sort close-date

        # Sort based on latest close date
        $ cat grants.csv | fed_sort close-date --reverse


         
*****************
fed_download_opps
*****************


**fed_extract_attr**
 
