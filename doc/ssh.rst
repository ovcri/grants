Setup
=====

***
SSH
***

To use the commands, first SSH into S&T's Linux Virtual Machines

1. Press Start and search cmd.exe
2. In the command prompt, type ``ssh <sso>@rc<NN>xcs213.managed.mst.edu``, where *<sso>* is your university login id, and *<NN>* is a number  between 01 and 30. So 'rc07xcs213.managed.mst.edu' is ok but 'rc7xcs213.managed.mst.edu' is not.
3. Change directory to SDRIVE/Downloads by typing ``cd ~/SDRIVE/Downloads``


*******
Install
*******

In the Linux Virtual Machine, install the grants package from the command line

``pip3 install --user <tbd>``

Then check if the commands are available. Typing

``which fed_download``

Should print out

``/usr/local/home/<sso>/.local/bin/fed_download``

If it does not, you need to set your environment path

``cat 'export PATH=$HOME/.local/bin:$PATH' >> ~/.bashrc``

Then, start a new instance of the terminal by typing

``bash``

Recheck if the scripts have been installed

``which fed_download``


The next time you login, you should not need to set
your environment path again.


